let arr1 = [1,2,3,4,5,6,7,8]

function chunk(array, value){
    let result = []
    const size = Math.ceil(value)
    if(size < 0 || size === 0){
        return result
    }
    while(array.length > 0){
        if(array.length > size){
           result.push(array.splice(0,size))
        } else{
            result.push(array.splice(0,array.length))
        }
    }
    console.log(result);
    return result
}

chunk(arr1,)